<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);

$dbcon = array(
            'host'=>'localhost',
            'user'=>'root',
            'pass'=>'',
            'db'=>'hersindo',
    );


require_once __DIR__.'/../vendor/autoload.php';
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application();

// conecting database 
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql_read' => array(
            'driver'    => 'pdo_mysql',
            'host'      => $dbcon['host'],
            'dbname'    => $dbcon['db'],
            'user'      => $dbcon['user'],
            'password'  => $dbcon['pass'],
            'charset'   => 'utf8',
        ),
    ),
));

include 'get_setting.php';

/* Global constants */
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH', dirname(ROOT_PATH).DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR);
define('ASSETS_PATH', ROOT_PATH.DIRECTORY_SEPARATOR);

// Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register Swiftmailer
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

// Register URL Generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Register Validator
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app['config_db'] = $dbcon;

$app['list_data_service'] = $list_data_service;
$app["twig"]->addGlobal("list_data_service", $list_data_service);

$app['data_estimate'] = $data_estimate;
$app["twig"]->addGlobal("data_estimate", $data_estimate);

$app['setting_email'] = $setting_email;
$app['setting_wa'] = $setting_wa;

// ------------------ Homepage ------------------------
$app->get('/', function () use ($app) {
	return $app['twig']->render('page/home.twig', array(
        'layout' => 'layouts/column1.twig',
        // 'benefits' => $app['data_benefits'],
    ));
})
->bind('homepage');

// ------------------ about ------------------
$app->get('/about', function () use ($app) {
    return $app['twig']->render('page/about.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('about');

// ------------------ services ------------------
$app->get('/services', function () use ($app) {
    $jumlahData = count($app['list_data_service']) - 1;

    return $app['twig']->render('page/service.twig', array(
        'layout' => 'layouts/inside.twig',
        'jumlah' => $jumlahData,
    ));
})
->bind('services');

// ------------------ network ------------------
$app->get('/network', function () use ($app) {

    return $app['twig']->render('page/network.twig', array(
        'layout' => 'layouts/inside.twig',
    ));
})
->bind('network');

// ------------------ contact ------------------
$app->match('/contact', function (Request $request) use ($app) {

    $data = $request->get('Contact');
    if ($data == null) {
        $data = array(
            'name'=>'',
            'phone'=>'',
            'company'=>'',
            'email'=>'',
            'address'=>'',
            'message'=>'',
        );
    }

    if ($_POST) {

         if (!isset($_POST['g-recaptcha-response'])) {
            return $app->redirect($app['url_generator']->generate('contact').'?msg=error_message');
        }
        $secret_key = "6Ld-ByoTAAAAAIJFgvxHwt_ZZX1OdG5KGExB_u_e";
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
        
        $response = json_decode($response);
        if($response->success==false)
        {
          return $app->redirect($app['url_generator']->generate('contact').'?msg=error_message');
        }else{

        $constraint = new Assert\Collection( array(
            'name' => new Assert\NotBlank(),
            'email' => array(new Assert\Email(), new Assert\NotBlank()),
            'phone' => new Assert\Length(array('max'=>2000)),
            'company' => new Assert\Length(array('max'=>2000)),
            'address' => new Assert\Length(array('max'=>2000)),
            'message' => new Assert\Length(array('max'=>2000)),
        ) );

        $errors = $app['validator']->validateValue($data, $constraint);

        $errorMessage = array();
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
            }
        } else {
             $app['swiftmailer.options'] = array(
                    'host' => 'mail.hersindo.com',
                    'port' => '25',
                    'username' => 'no-reply@hersindo.com',
                    'password' => '1q2w3e4r5t6y',
                    'encryption' => null,
                    'auth_mode' => login
                );

             // echo $app['twig']->render('page/mail.twig', array(
             //        'data' => $data,
             //    ));
            
            $pesan = \Swift_Message::newInstance()
                ->setSubject('Hi, Contact Website Hersindo Logistic')
                ->setFrom(array('no-reply@hersindo.com'))
                ->setTo( array('info@hersindo.com', $data['email']) )
                ->setBcc( array('deoryzpandu@gmail.com', 'ibnu@markdesign.net') )
                ->setReplyTo(array('info@hersindo.com '))
                ->setBody($app['twig']->render('page/mail.twig', array(
                    'data' => $data,
                )), 'text/html');

            $app['mailer']->send($pesan);
            return $app->redirect($app['url_generator']->generate('contact').'?msg=success');
            }
        }
        // else captcha
    }

    // exit;
    return $app['twig']->render('page/contactus.twig', array(
        'layout' => 'layouts/inside.twig',
        'error' => $errorMessage,
        'data' => $data,
        'msg' =>$_GET['msg'],
    ));
})
->bind('contact');

// ------------------ estimates ------------------
$app->match('/estimates', function (Request $request) use ($app) {

    // Create connection
    $conn = new mysqli($app['config_db']['host'], $app['config_db']['user'], $app['config_db']['pass'], $app['config_db']['db']);

    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    $data = $request->get('Estimate');
    if ($data == null) {
        $data = array(
            'from' => '',
            'to' => '',
            'type_ship' => '',
            'type_load' => '',
            'goods' => '',
            'qty' => '',
            'container_type' => '',
            'dates' => '',
            'pickup_from' => '',
            'ship_to' => '',
            'name' => '',
            'company' => '',
            'phone' => '',
            'email' => '',
            'from_home' => '',
            'body' => '',
        );
    }

    if (isset($_POST['Estimate'])) {

        //  if (!isset($_POST['g-recaptcha-response'])) {
        //     return $app->redirect($app['url_generator']->generate('estimates').'?msg=error_message');
        // }

        $secret_key = "6Ld-ByoTAAAAAIJFgvxHwt_ZZX1OdG5KGExB_u_e";
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
        $response = json_decode($response);
        if($response->success==false)
        {
            if (empty($_POST['Estimate']['from_home'])) {
                return $app->redirect($app['url_generator']->generate('home').'?msg=error_message');
            }
        }else{

            $constraint = new Assert\Collection( array(
                'from' =>  new Assert\Length(array('max'=>225)),
                'to' =>  new Assert\Length(array('max'=>225)),
                'type_ship' =>  new Assert\Length(array('max'=>225)),
                'type_load' =>  new Assert\Length(array('max'=>225)),
                'goods' =>  new Assert\Length(array('max'=>225)),
                'qty' =>  new Assert\Length(array('max'=>225)),
                'container_type' =>  new Assert\Length(array('max'=>225)),
                'dates' =>  new Assert\Length(array('max'=>225)),
                'pickup_from' =>  new Assert\Length(array('max'=>225)),
                'ship_to' =>  new Assert\Length(array('max'=>225)),
                'name' => new Assert\NotBlank(),
                'company' => new Assert\Length(array('max'=>225)),
                'phone' => new Assert\Length(array('max'=>225)),
                'email' => new Assert\Length(array('max'=>225)),
            ) );

            $errors = $app['validator']->validateValue($data, $constraint);

            $errorMessage = [];
            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessage[] = $error->getPropertyPath().' '.$error->getMessage();
                }
            } else {
                 $app['swiftmailer.options'] = array(
                        'host' => 'mail.hersindo.com',
                        'port' => '25',
                        'username' => 'no-reply@hersindo.com',
                        'password' => '1q2w3e4r5t6y',
                        'encryption' => null,
                        'auth_mode' => login
                    );
                    
                $pesan = \Swift_Message::newInstance()
                    ->setSubject('Hi, Estimate Website Hersindo Logistic')
                    ->setFrom(array('no-reply@hersindo.com'))
                    ->setTo( array($app['setting_email']['value'], $data['email']) )
                    ->setBcc( array('ibnu@markdesign.net') )
                    ->setReplyTo(array($app['setting_email']['value']))
                    ->setBody($app['twig']->render('page/mail_estimates.twig', array(
                        'data' => $data,
                    )), 'text/html');
                $app['mailer']->send($pesan);

                $sn_date = date('Y-m-d', strtotime($data['dates']));
                // save db
                $sql_insert = "INSERT INTO `tb_estimate` (`from`, `to_address`, `type_ship`, `type_load`, `goods`, `qty`, `container_type`, `dates`, `pickup_from`, `ship_to`, `name`, `company`, `phone`, `email`)
                VALUES ('".$data['from']."', '".$data['to']."', '".$data['type_ship']."', '".$data['type_load']."', '".$data['goods']."', '".$data['qty']."', '".$data['container_type']."', '".$sn_date."', '".$data['pickup_from']."', '".$data['ship_to']."', '".$data['name']."', '".$data['company']."', '".$data['phone']."', '".$data['email']."')";

                $conn->query($sql_insert);
                $conn->close();

                $nmail = $data['email'];
                return $app->redirect($app['url_generator']->generate('estimates').'?msg=success&email='. $nmail);
            }
        }
        // else captcha
    }

    // get data success
    $n_wa_calls = '';
    if ( (isset($_GET['msg']) && $_GET['msg'] == 'success') and $_GET['email'] != null ) {
        $n_email = $_GET['email'];
        $model = $app['db']->fetchAssoc('SELECT * FROM tb_estimate where email = "'.$n_email.'" order by id desc');
        
    $wa_message = 'ESTIMASI KIRIMAN HERSINDO 
FROM '.strtoupper($model['from']).' TO '.strtoupper($model['to_address']).'
'.$model['type_ship'].'
TYPE OF CONTAINER: '.$model['type_load'].'
TYPE OF GOODS: '.$model['goods'].'
QTY '.$model['qty'].' '.$model['container_type'].'

ESTIMATED DATE OF DELIVERY: '.date('d F Y', strtotime($model['dates'])).'

'.strtoupper($model['from']).' ADDRESS:
'.$model['pickup_from'].'

'.strtoupper($model['to_address']).' ADDRESS:
'.$model['ship_to'].'

CONTACT DETAIL:
'.$model['name'].'
'.$model['company'].'
'.$model['email'];

    $wa_messagenn = rawurlencode($wa_message);

    $n_wa_calls = 'https://wa.me/'.$app['setting_wa']['value'].'?text='. $wa_messagenn;
    }

    return $app['twig']->render('page/estimates.twig', array(
        'layout' => 'layouts/inside.twig',
        'error' => $errorMessage,
        'data' => $data,
        'msg' => isset($_GET['msg']) ? $_GET['msg'] : null,
        'wa_message' => $n_wa_calls,
    ));
})
->bind('estimates');

$app['debug'] = true;

$app->run();