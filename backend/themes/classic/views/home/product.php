<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Products</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 <section class="home-sec-products">
   <div class="prelatife container">
        <div class="row">
            <div class="col-md-60">
                 <div class="inner-judul">
                   <h3>Our Seafood Products</h3>
                   <p>PT. Bumi Menara Internusa is supplying a wide range of shrimp, fish, crabs and value added seafood products from Indonesia, including raw and cooked sea food products such as White Shrimp, Squid, Cuttlefish, Baby Octopus, Seafood Mix, Seafood Skewers, Tuna Loins and many more.</p>
                 </div>
             </div>
        </div>
    </div>
</section>