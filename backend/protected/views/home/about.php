<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife">
        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,735, '/images/static/'. $this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
    </div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1><?php echo $this->setting['about_hero_title'] ?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $this->setting['about_hero_title'] ?></li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a onclick="window.history.back();" href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-about-1">
    <div class="py-4"></div>
    <div class="prelative container">
        <div class="row list_data-about-1">
            <div class="col-md-30">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about1_image'] ?>" alt="">
                </div>
            </div>
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <?php echo $this->setting['about1_content'] ?>
                </div>
            </div>
        </div>
    </div>
    <div class="prelative container">
        <div class="row list_data-about-2">
            
            <?php for ($i=1; $i < 4; $i++) { ?>
            <div class="col-md-20">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about2_banners_'. $i] ?>" alt="">
                </div>
            </div>
            <?php } ?>

        </div>
    </div>
    <div class="prelative container">
        <div class="row list_data-about-3">
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <?php echo $this->setting['about3_content'] ?>
                    <img src="<?php echo $this->assetBaseurl; ?>logo-sap.png" class="img img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-30">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about3_image'] ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-sec-about-2">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-60">
                <div class="outers">
                    <div class="content content-texts">
                        <h4><?php echo $this->setting['about4_title'] ?></h4>
                        <div class="outer row">
                            <div class="col-md-10"></div>
                            <div class="col-md-40 text-center">
                                <?php echo $this->setting['about4_subcontent'] ?>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="prelative container">
        <div class="row list-data-about">
            <?php for ($i=1; $i < 5; $i++) { ?>
            <div class="col-md-15">
                <div class="outerss">
                    <div class="pictures">
                        <img class="img img-fluid d-block mx-auto" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['about4_hl_picture_'. $i] ?>" alt="">
                    </div>
                    <div class="jarak">
                        <div class="content">
                            <h4><?php echo $this->setting['about4_hl_titles_'. $i] ?></h4>
                            <div class="py-1"></div>
                            <p><?php echo $this->setting['about4_hl_desc_'. $i] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="home-sec-about-3 outers_back_visionblock py-5">
    <div class="py-4"></div>
    <div class="prelatife container">
        <div class="row">
            <div class="col-md-30">
                <div class="content">
                    <h4>Vision</h4>
                    <?php echo $this->setting['about_visi'] ?>
                </div>
            </div>
            <div class="col-md-30">
                <div class="content">
                    <h4>Mision</h4>
                    <?php echo $this->setting['about_misi'] ?>
                </div>
            </div>
        </div>
        <div class="clearfix clear"></div>
    </div>
</section>