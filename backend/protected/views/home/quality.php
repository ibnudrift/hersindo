<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife">
        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,735, '/images/static/'. $this->setting['quality_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img img-fluid">
    </div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1><?php echo $this->setting['quality_hero_title'] ?></h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $this->setting['quality_hero_title'] ?></li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-quality-1">
    <div class="prelative container">
        <div class="row list_data-1">
            <div class="col-md-30">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality1_image'] ?>" alt="">
                </div>
            </div>
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <?php echo $this->setting['quality1_content'] ?>
                </div>
            </div>
        </div>
        <div class="row list_data-2">
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <?php echo $this->setting['quality2_content'] ?>
                </div>
            </div>
            <div class="col-md-30">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality2_image'] ?>" alt="">
                </div>
            </div>
        </div>
        <div class="row list_data-3">
            <div class="col-md-30">
                <div class="pictures">
                    <img class="img img-fluid w-100" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality3_image'] ?>" alt="">
                </div>
            </div>
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <?php echo $this->setting['quality3_content'] ?>
                    <!-- <img src="<?php echo $this->assetBaseurl; ?>logo-sap.png" class="img img-fluid" alt=""> -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-quality-2 back-quality_outerbottom" >
    <div class="prelative container">
        <div class="py-5"></div>
        <div class="py-4 d-none d-sm-block"></div>
        <div class="inner-content content-texts text-center">
            <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-40">
                    <h3><?php echo $this->setting['quality4_title'] ?></h3>
                    <h5><?php echo $this->setting['quality4_subcontent'] ?></h5>
                </div>
                <div class="col-md-10"></div>
            </div>
        </div>

        <div class="box-image">
            <h3>The BMI quality process commitment:</h3>
            <div class="row lists_data qualitys">

                <?php for ($i=1; $i < 5; $i++) { ?>
                <div class="col-md-15 col-30">
                    <div class="pictures">
                        <img class="img img-fluid d-block mx-auto" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['quality4_hl_picture_'. $i] ?>" alt="">
                    </div>
                    <div class="contents1">
                        <h4><?php echo $this->setting['quality4_hl_titles_'. $i] ?></h4>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>