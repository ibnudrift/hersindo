<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Products</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a onclick="window.history.back();" href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="insides_page pg_default pg-machine">
    <div class="prelatife container">
        <div class="content-texts inners text-center">
            <div class="py-5"></div>
            
            <div class="text-center tops_titledetail">
                <h2>Products</h2>
            </div>
            <div class="py-2"></div>
            <div class="tops_filter">
                <div class="row">
                    <div class="col-md-20">
                        <form class="form-inline">
                          <label for="inlineForm">See other products</label>
                          <?php 
                            $criteria = new CDbCriteria;
                            $criteria->with = array('description');
                            $criteria->addCondition('t.type = :type');
                            $criteria->params[':type'] = 'category';
                            // $criteria->limit = 3;
                            $criteria->order = 't.sort ASC';
                            $nCategory = PrdCategory::model()->findAll($criteria);
                          ?>
                          <select class="change_cat" name="categorys" id="">
                              <option value="">Select</option>
                              <?php foreach ($nCategory as $key => $value): ?>
                                <option value="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'category'=> $value->id)); ?>"><?php echo $value->description->name ?></option>
                              <?php endforeach ?>
                          </select>
                        </form>
                        <script type="text/javascript">
                        $(function(){
                            $('.change_cat').change(function(){
                                var sn_url = $(this).val();
                                window.open(sn_url, "_SELF");
                                return false;
                            });
                        });
                        </script>
                    </div>
                    <div class="col-md-20">
                        <p class="m-0 titles_prd"><?php echo $category->description->name ?></p>
                    </div>
                    <div class="col-md-20 text-right">
                        <a href="#" onclick="window.history.back();" class="btn btn-link p-0 backs_to">Back to product category</a>
                    </div>
                </div>
            </div>
            <div class="py-3 my-4"></div>

            <div class="lists_detail_products">
                <?php if ( count( $products->getData() ) > 0 ): ?>
                    <?php foreach ($products->getData() as $key => $value): ?>
                    <div class="items">
                        <div class="row no-gutters">
                            <div class="col-md-30">
                                <div class="pictures back-white">
                                    <img src="<?php echo Yii::app()->baseUrl.'/images/product/' ?><?php echo $value->image ?>" alt="" class="img img-fluid d-block mx-auto">
                                </div>
                            </div>
                            <div class="col-md-30 back-grey my-auto text-left p-4">
                                <div class="descriptions p-5">
                                    <h4><?php echo $value->description->name ?></h4>
                                    <div class="py-3"></div>
                                    <span>PRODUCT DESCRIPTION</span>
                                    <?php echo $value->description->desc; ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?php else: ?>
                    <h4>Sorry, data is empty!</h4>
                <?php endif ?>

            </div>

            <div class="py-5"></div>
            <div class="py-2"></div>

            <div class="clear clearfix"></div>
        </div>
    </div>
</section>