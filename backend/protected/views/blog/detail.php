<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Blogs & Articles</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs & Articles</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-detail-cnt1">
    <div class="prelatife container w904">
      <div class="inners py-5">
        <div class="tops_title text-center pb-4 mb-2">
          <p>BLOGS & ARTICLES</p>
          <div class="py-2"></div>
          <h1><?php echo $dataBlog->description->title; ?></h1>
        </div>
        <div class="py-1"></div>

        <div class="inner-img">
          <img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/blog/'. $dataBlog->image ?>" alt="">
        </div>
        <div class="inner-isi content-texts pt-4 mt-3">
          <?php echo $dataBlog->description->content; ?>

          <!-- <div class="text-center">
            <div class="py-3"></div>
            <div class="tengah buttons_blue d-block mx-auto">
              <a href="#" class="btn btn-info btns_blue_host">Next Blogs & Articles</a>
            </div>
          </div> -->
        </div>
      </div>
      <div class="py-3"></div>
    </div>
  </section>
  <div class="py-3"></div>
  <div class="lines-grey"></div>
  <div class="py-5"></div>  

  <section class="home-sec-3 outers_block_articleshome mt-3">
   <div class="prelative container">
      <h4>Others Blog & Articles</h4>
      <div class="py-3 d-block d-sm-none"></div>
      <div class="box-image outers_list_news_data">

         <div class="row no-gutters lists_data justify-content-center">

            <?php foreach ($dataBlogs->getData() as $key => $value): ?>
            <div class="col-md-20 col-30">
              <div class="items">
                 <div class="pictures">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(432,261, '/images/blog/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" class="img img-fluid" alt=""></a>
                 </div>
                 <div class="contents">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>"><p><?php echo $value->description->title; ?></p></a>
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=> $value->id)); ?>" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <?php endforeach ?>

         </div>

      </div>
   </div>
</section>
<div class="py-4"></div>
<div class="py-3"></div>

<?php
/*
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">BLOGS</h1>
        <div class="clear"></div>
        <h3 class="tagline"><?php echo $dataBlog->description->title ?></h3>
        <div class="clear"></div>
        <div class="row details_cont_articles">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">

                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,1000, '/images/blog/'.$dataBlog->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="">

                <?php echo $dataBlog->description->content ?>

                <div class="clear height-10"></div>
                <div class="shares-text text-left p_shares_article">
                    <span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=#">FACEBOOK</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://plus.google.com/share?url=#">GOOGLE PLUS</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://twitter.com/home?status=#">TWITTER</a>
                </div>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">Other Blogs</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                    <?php foreach ($dataBlogs->getData() as $key => $value): ?>
                        
                  <li><a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></li>
                    <?php endforeach ?>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>
      
      <div class="clear height-20"></div>
      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/
?>