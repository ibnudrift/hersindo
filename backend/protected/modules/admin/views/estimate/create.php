<?php
$this->breadcrumbs=array(
	'Estimates'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Estimate',
	'subtitle'=>'Add Estimate',
);

$this->menu=array(
	array('label'=>'List Estimate', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>