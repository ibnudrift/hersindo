<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
	<?php echo CHtml::encode($data->from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to_address')); ?>:</b>
	<?php echo CHtml::encode($data->to_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_ship')); ?>:</b>
	<?php echo CHtml::encode($data->type_ship); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_load')); ?>:</b>
	<?php echo CHtml::encode($data->type_load); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('goods')); ?>:</b>
	<?php echo CHtml::encode($data->goods); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qty')); ?>:</b>
	<?php echo CHtml::encode($data->qty); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('container_type')); ?>:</b>
	<?php echo CHtml::encode($data->container_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dates')); ?>:</b>
	<?php echo CHtml::encode($data->dates); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pickup_from')); ?>:</b>
	<?php echo CHtml::encode($data->pickup_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ship_to')); ?>:</b>
	<?php echo CHtml::encode($data->ship_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company')); ?>:</b>
	<?php echo CHtml::encode($data->company); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body')); ?>:</b>
	<?php echo CHtml::encode($data->body); ?>
	<br />

	*/ ?>

</div>