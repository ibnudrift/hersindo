<?php
$this->breadcrumbs=array(
	'Estimates'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Estimate','url'=>array('index')),
	array('label'=>'Add Estimate','url'=>array('create')),
);
?>

<h1>Manage Estimates</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'estimate-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'from',
		'to_address',
		'type_ship',
		'type_load',
		'goods',
		/*
		'qty',
		'container_type',
		'dates',
		'pickup_from',
		'ship_to',
		'name',
		'company',
		'phone',
		'email',
		'body',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
