<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'estimate-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Estimate</h4>
<div class="widgetcontent">


	<?php echo $form->textFieldRow($model,'from',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'to_address',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'type_ship',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'type_load',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'goods',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'qty',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'container_type',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'dates',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'pickup_from',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'ship_to',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'body',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php 
			// $this->widget('bootstrap.widgets.TbButton', array(
			// 	'buttonType'=>'submit',
			// 	'type'=>'primary',
			// 	'label'=>$model->isNewRecord ? 'Add' : 'Save',
			// )); 
		?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
