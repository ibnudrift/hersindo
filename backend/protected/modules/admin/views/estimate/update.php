<?php
$this->breadcrumbs=array(
	'Estimates'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Estimate',
	'subtitle'=>'Edit Estimate',
);

$this->menu=array(
	array('label'=>'List Estimate', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Estimate', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Estimate', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>