<?php
$this->breadcrumbs=array(
	'Estimates',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Estimate',
	'subtitle'=>'Data Estimate',
);

$this->menu=array(
	// array('label'=>'Add Estimate', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'alerts'=>array('success'),
    )); ?>

<?php endif; ?>
<h1>Estimate</h1>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'estimate-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'enableSorting'=>false,
	'summaryText'=>false,
	'type'=>'bordered',
	'columns'=>array(
		// 'id',
		'from',
		'to_address',
		'type_ship',
		'type_load',
		'goods',
		'name',
		'email',
		/*
		'qty',
		'container_type',
		'dates',
		'pickup_from',
		'ship_to',
		'company',
		'phone',
		'body',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} &nbsp; {delete}',
		),
	),
)); ?>
