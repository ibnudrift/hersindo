<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'from',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'to_address',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'type_ship',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'type_load',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'goods',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'qty',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'container_type',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'dates',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'pickup_from',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'ship_to',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textAreaRow($model,'body',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
