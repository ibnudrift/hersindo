<?php

/**
 * This is the model class for table "tb_estimate".
 *
 * The followings are the available columns in table 'tb_estimate':
 * @property string $id
 * @property string $from
 * @property string $to_address
 * @property string $type_ship
 * @property string $type_load
 * @property string $goods
 * @property integer $qty
 * @property string $container_type
 * @property string $dates
 * @property string $pickup_from
 * @property string $ship_to
 * @property string $name
 * @property string $company
 * @property string $phone
 * @property string $email
 * @property string $body
 */
class Estimate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Estimate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_estimate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qty', 'numerical', 'integerOnly'=>true),
			array('from, to_address, type_ship, type_load, goods, container_type, name, company, phone, email', 'length', 'max'=>225),
			array('dates, pickup_from, ship_to, body', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, from, to_address, type_ship, type_load, goods, qty, container_type, dates, pickup_from, ship_to, name, company, phone, email, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from' => 'From',
			'to_address' => 'To Address',
			'type_ship' => 'Type Ship',
			'type_load' => 'Type Load',
			'goods' => 'Goods',
			'qty' => 'Qty',
			'container_type' => 'Container Type',
			'dates' => 'Dates',
			'pickup_from' => 'Pickup From',
			'ship_to' => 'Ship To',
			'name' => 'Name',
			'company' => 'Company',
			'phone' => 'Phone',
			'email' => 'Email',
			'body' => 'Body',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to_address',$this->to_address,true);
		$criteria->compare('type_ship',$this->type_ship,true);
		$criteria->compare('type_load',$this->type_load,true);
		$criteria->compare('goods',$this->goods,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('container_type',$this->container_type,true);
		$criteria->compare('dates',$this->dates,true);
		$criteria->compare('pickup_from',$this->pickup_from,true);
		$criteria->compare('ship_to',$this->ship_to,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('body',$this->body,true);

		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}