<?php 
// function get database $dbcon
// mysql_connect($dbcon['host'], $dbcon['user'], $dbcon['pass']);
// mysql_select_db($dbcon['db']);

    function getRandom()
    {
        $randm = 'rand-'.rand(1000,9999);
        return $randm;
    }

    $set_random = getRandom();
    
    $list_data_service = array(
            array(
                'title' => 'Door to Door All In',
                'desc' => 'Hersindo logistic team will pick up and deliver cargo, along with unloading goods to designated warehouse at a timely manner with directional coordination.',
                ),
            array(
                'title' => 'Door to Door Free On Truck',
                'desc' => 'Hersindo logistic team will pickup and deliver cargo to designated warehouse, while the unloading process will be done by the warehouse client.',
                ),
            array(
                'title' => 'Port to Port',
                'desc' => 'Hersindo logistic team will load cargo at origin port&rsquo;s warehouse and unload at the designated port&rsquo;s warehouse. This service will be accompanied by accurate information and coordination from the time the ship departs until the time the ship arrives at the designated port.',
                ),
            array(
                'title' => 'Port to Door',
                'desc' => 'Hersindo logistic team will load cargo at origin port&rsquo;s warehouse and unload at the designated port&rsquo;s warehouse, and Hersindo logistic team will deliver the cargo to the designated address.',
                ),
            array(
                'title' => 'International Ocean Freight - Project Cargo',
                'desc' => 'Hersindo logistic will arrange your cargo from pickup and delivery, and manage shipping documentation. With decades of experience, we facilitate the entire process according to your specifications and the applicable requirements of the import and export countries. We will cater customised project solution for movement of specialized equipment or involving specialized charter vessels with global coverage at a reasonable costs.',
                ),
            array(
                'title' => 'Import / Export & Custom Clearance',
                'desc' => 'Hersindo logistic team will cater to all your import / export activities, along with working to prepare and submit of documentations required to facilitate export or imports into the country, representing client during customs examination, assessment, payment of duty and co taking delivery of cargo from customs after clearance along with documents.',
                ),

        );

        $list_data_service =  array_chunk($list_data_service, 2);



        $data_estimate = [
                            'container' =>[
                                            'CONTAINER 20 FT',
                                            'CONTAINER 40 FT',
                                            'CONTAINER 21 FT',
                                            'CONTAINER 40 FT HC',
                                            'ISO TANK 20 FT',
                                          ],
                            'goods' =>[
                                        'ACCU BEKAS',
                                        'AIR MINUM',
                                        'ASPAL',
                                        'BAHAN BANGUNAN',
                                        'BAHAN KIMIA',
                                        'BERAS',
                                        'BESI',
                                        'GARAM',
                                        'GULA',
                                        'GYPSUM',
                                        'HCL KLORIDE',
                                        'KEDELAI',
                                        'KOPRA',
                                        'MAKANAN',
                                        'MIE',
                                        'MINYAK GORENG',
                                        'MOBIL',
                                        'OLI',
                                        'PAKAN TERNAK ',
                                        'PUPUK CAIR',
                                        'PUPUK PADAT',
                                        'ROKOK',
                                        'SEMEN',
                                        'SNACK',
                                        'TABUNG GAS',
                                        'TEPUNG',
                                        'TRIPLEK',
                                        'LAINNYA',
                                      ],
                            'loads' =>['FCL', 'LCL'],
                            'qty_fcl' =>[
                                            '1',
                                            '2 ',
                                            '3',
                                            '4',
                                            '5',
                                            '> 5',
                                        ],
                            'qty_lcl' =>[
                                            '1 TON',
                                            '2 TON',
                                            '3 TON',
                                            '4 TON',
                                            '5 TON',
                                            'LEBIH DARI 5 TON',
                                        ],
                         ];


        $setting_email = $app['db']->fetchAssoc('SELECT * FROM setting WHERE `name` = "email"');
        
        $setting_wa = $app['db']->fetchAssoc('SELECT * FROM setting WHERE `name` = "enquire_wa"');     
